<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrabajadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trabajadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trabajadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,//busqueda
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'nombre',
            'apellidos',
            'fechaNacimiento',
             [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                        return Html::img("@web/imgs/$data->foto",['class'=>'img-responsive','style'=>'width:200px;']); 
                       }
            ],
            'delegacion',
//            'delegacion0.nombre',
            [
                'attribute' => 'nombredelegacion',
                'value'=>'delegacion0.nombre',
                
            ],
            [
                'attribute' => 'poblacion',
                'value'=>'delegacion0.poblacion',
                
            ],
            
            [
                'class' => 'yii\grid\ActionColumn',
                 'template' => '{view} {update} {delete} {link}',
                'buttons' => [
                
                'link' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-menu-right"></span>',['delegacion/view',"id"=>$model->delegacion],['class'=>'link'] );
                },
                ],
            ],
                    
        ],
    ]);
    ?>
</div>
