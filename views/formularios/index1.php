<?php
use yii\widgets\DetailView;
?>
<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'nombre',
            'apellidos',
            'edad',
            'peso',
            'altura',
            'poblacion',
            [
            'attribute'=>"nombreCompleto",    
            'value'=>function($model){
                return $model->getNombreCompleto();
            }
            ],
            [
            'attribute'=>"IMC",    
            'value'=>function($model){
                return $model->getImc();
            }
            ],
        ],
    ]) ?>

