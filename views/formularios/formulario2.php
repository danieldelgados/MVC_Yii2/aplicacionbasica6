<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$form = ActiveForm::begin([
    'id' => 'formulario2',
    'options' => ['class' => 'form-horizontal'],
]); ?>
    <?=$form->errorSummary($model)?>;
    <?= $form->field($model, 'nombre') ?>
    <?= $form->field($model, 'apellidos')?>
    <?= $form->field($model, 'edad')?>
    <?= $form->field($model, 'peso')?>
    <?= $form->field($model, 'altura')?>
    <?= $form->field($model, 'poblacion')->dropdownList($model->getValores(),
    ['prompt'=>'Selecciona población']
);?>
    
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>

