<?php

namespace app\controllers;
use yii\web\Controller;
use Yii;
use app\models\Formulario;

class FormulariosController extends Controller{
    
    public function actionIndex(){
        $id=Yii::$app->request->post();
        if($id){
            $mensaje=$id;
        }else{
            return $this->redirect(["formularios/formulario"]);
        }
        
        return $this->render('index',[
            "mensaje"=>'Probando formularios de Yii2',
            "mensaje"=>$mensaje,
           
        ]);
    }
    public function actionFormulario(){
               
        
        return $this->render('formulario',[
            
        ]);
    }
    
    public function actionFormulario1(){
        $model=new Formulario();
        $dato=Yii::$app->request->post();
        if($dato){
        $model->load(Yii::$app->request->post());
        return $this->render("index",[
            "mensaje"=>$model,
        ]);
        }
        
        return $this->render('formulario1',[
            "model"=>$model,
        ]);
    }
    public function actionFormulario2(){
        $model=new Formulario();
        $dato= Yii::$app->request->post();
        if($dato){
            $model->load($dato);
            if($model->validate()){
            $nombreCompleto=$model->getNombreCompleto();
            $imc=$model->getImc();
            return $this->render('index1',[
                "model"=>$model,
            ]);
            
            }
        }
        return $this->render('formulario2',[
            
            "model"=>$model,
        ]);
    }
    
    
    
    
    
    
}