<?php
namespace app\models;
use Yii;
use yii\base\Model;

class Formulario extends Model{
    public $nombre;
    public $edad;
    public $apellidos;
    public $peso;
    public $altura;
    public $poblacion;


    public function rules(){// nos permite definir las reglas de negocio
        
        return [
          [['nombre','edad','peso','altura'],"required",'message'=>"El campo {attribute} no puede estar vacio"],
            ['edad','integer','message'=>'El campo edad debe ser numerico'],
            ['nombre','string','max'=>20],
            ['peso','integer'],
            ['altura','integer','min'=>100,'max'=>250,"message"=>"El campo {attribute} no debe llevar comas ni puntos"],
            ['apellidos','string','max'=>50],
            [['nombre','edad',"poblacion","altura","peso","apellidos"],"safe"],
        ];
    }
    public function attributeLabels() {
        return[
          'nombre'=>'Nombre del trabajador',
          'edad'=>'Edad del trabajador',
          'apellidos'=>'Apellidos del trabajador',
          'peso'=>'Peso del trabajador',
          'altura'=>'Altura del trabajador',
          'poblacion'=>'Poblacion del trabajador',
        ];
        
    }
    
    public function getNombreCompleto(){
        $nombre= $this->nombre;
        $apellido= $this->apellidos;
        
        return $nombre." ".$apellido ;
    }
    
    public function getValores(){
        return[
            "Santander"=>"Santander",
            "Potes"=>"Potes",
            "Laredo"=>"Laredo"
        ];
    }
    
    public  function getImc(){
        $peso= $this->peso;
        $altura= $this->altura;
        
        return $peso / ($altura*$altura)*10000;
                
        
    }
}

