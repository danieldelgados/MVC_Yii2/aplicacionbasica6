<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trabajadores;

/**
 * TrabajadoresSearch represents the model behind the search form of `app\models\Trabajadores`.
 */
class TrabajadoresSearch extends Trabajadores
{   
    public $poblacion;
    public $nombredelegacion;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'delegacion'], 'integer'],
            [['nombre', 'apellidos', 'fechaNacimiento', 'foto','poblacion','nombredelegacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trabajadores::find();
        

        // add conditions that should always apply here
        $query->joinWith(["delegacion0"]); 

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
              'pageSize'=>2,
                
            ],
        ]);
        $dataProvider->sort->attributes['nombredelegacion']=[
            'asc'=>['delegacion.nombre'=>SORT_ASC],
            'desc'=>['delegacion.nombre'=>SORT_DESC],
                ];
        $dataProvider->sort->attributes['poblacion']=[
            'asc'=>['delegacion.poblacion'=>SORT_ASC],
            'desc'=>['delegacion.poblacion'=>SORT_DESC],
                ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fechaNacimiento' => $this->fechaNacimiento,
            'delegacion' => $this->delegacion,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'delegacion.nombre', $this->nombredelegacion])
            ->andFilterWhere(['like', 'delegacion.poblacion', $this->poblacion]);

        return $dataProvider;
    }
}
